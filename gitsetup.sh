basedir=$PWD
base="git@gitlab.com:higenku"
clone_and_setup() {
    repo=$1
    git clone "$base/$repo.git" $repo
    cd $repo
    git checkout wip || git checkout -b wip
    cd $basedir 
}

mkdir -p apps sdks tools run

## Apps

clone_and_setup apps/account
clone_and_setup apps/client
clone_and_setup apps/docs
clone_and_setup apps/redray
clone_and_setup apps/site
clone_and_setup apps/store
clone_and_setup apps/suite

## Tools
clone_and_setup tools/bak
clone_and_setup tools/packer
clone_and_setup tools/man
clone_and_setup tools/pacman

## Packs
clone_and_setup packs

## Sdks
clone_and_setup sdks/db
clone_and_setup sdks/keyring
clone_and_setup sdks/theme
clone_and_setup sdks/krome
